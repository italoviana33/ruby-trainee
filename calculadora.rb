require_relative 'operacoes_extras'
require 'net/http'
require 'json'

module Calculadora

  class Operacoes
    include OperacoesExtras

    def media_preconceituosa(notas, lista_negra) # media prenconseituosa
      notashash=JSON.parse(notas) #conversão 
      litasNegraSeparada= lista_negra.split(',') #separação da string
      notas_Sem_ListaNegra = notashash.select do |nota| #tira as pessoas que estão na lista negra
        not litasNegraSeparada.include? nota
      end
      result=0;
      notas_Sem_ListaNegra.each do |key,value| #soma o valor das notas
      result+=value;
      end
      result= (result.to_f/notas_Sem_ListaNegra.size.to_f); #media 
      puts "A media da turma é:"+result;
    end

    def sem_numeros(numeros) #inicio da função sem_numeros
        numeros_separados=numeros.split(" "); #separa os numeros 
        result = "";
        numeros_separados.each do |nume| #armazena o valor de sim ou não para caso divisivel
          if divisivel(nume)
            result +="Sim, "; 
          else
            result += "Não, ";
          end
        end
        puts result;
        print "\n\n";
        return;
    end #fim da função sem_numeros

    def divisivel(numero)
      ultimo = numero[-1]; #pega o ultimo caractere
      penultimo = numero[-2]; #pega o penultimo
      if ultimo == "0" || ultimo == "5" #verifica se o ultimo valor é 0 ou 5
        if penultimo == "7" || penultimo == "5" || penultimo == "2" || penultimo == "0" #verifica se o penultimo é 7, 5 ou 2
          return true;
        else
          return false;
        end
      else 
        return false;
      end 
    end

    def filtrar_filmes()
      filmes = get_filmes(); #faz o download do json
      puts "escolha um filtro para os filmes";
      print " 1- por genero\n";
      print " 2- por genero e ano\n";
      print " 3- sem filtro\n";
      print " 4- pesquisar por nome\n";
      var1=gets.chomp.to_i;
      case var1
      when 1
        puts filmes[:genres];
        puts "escolha um dos generos acima";
        var1=gets.chomp;
        filtro(filmes,"Genero",var1,"");
      when 2
        puts filmes[:genres];
        puts "escolha um dos generos acima";
        var2=gets.chomp;
        puts "qual o ano?";
        var3= gets.chomp;
        filtro(filmes,"GeneroAno",var2,var3);
      when 3
        filtro(filmes,"semFiltro","","");
      when 4
        puts "digite o nome do filme";
        var2=gets.chomp;
        filtro(filmes,"nomeFilme","",var2);
      when 0
        return;
      else
        puts "entrada invalida";
      end
    end
    def numPerf(p)
      quadPerf(p);
    end
    private
    def filtro(filmes,tipo,var1,var2)
      case tipo
      when "semFiltro"
        i=0;
        while i<filmes[:movies].size do
          print filmes[:movies][i][:title];
          print " * "
          if(i%4)==4
            print "\n"
          end
          i+=1
        end
        print "\n\n"
      when "nomeFilme"
        selectedfilme =""
        i=0
        while i<filmes[:movies].size do
          if var2 == (filmes[:movies][i][:title])
            selectedfilme = filmes[:movies][i]
          end
          i+=1
        end    
        if selectedfilme==""
          puts "Filme não encotrado!"                     
        else
          puts "------------FILME-SELECIONADO--------------------------------------------------------------"
          puts "titulo:         " + selectedfilme[:title]
          puts "ano:            " + selectedfilme[:year]
          puts "tempo de filme: " + selectedfilme[:runtime] + " minutos"
          puts "sinops: " + selectedfilme[:plot]
          puts "-------------------------------------------------------------------------------------------"
          gets.chomp
        end
      when "Genero"
        i=0;
        a=0;
        selectedfilme=""
        selectedfilme= filmes[:movies].select do |filme|
          filme[:genres].include? var1
        end
        print "---------------FILMES-SELECIONADO----------------------\n"
        i=0
        while i<selectedfilme.length do
          print selectedfilme[i][:title]
          print " * "
          if (i%4)== 0
            print "\n"
          end
          i+=1
        end
        print "\n"
        print "-------------------------------------------------------\n"
      when "GeneroAno"
        
        selectedfilme=""
        selectedfilme = filmes[:movies].select do |filme|
            filme[:genres].include? var1
        end
        seletedfilmesFINAL=""
        seletedfilmesFINAL= selectedfilme.select do |filme|
          filme[:year]>=var2
        end
        i=0
        puts "------------------------------FILME-SELECIONADOS-----------------------"
        while i<seletedfilmesFINAL.length do
          print seletedfilmesFINAL[i][:title]
          print " * "
          if (i%4)==0
          print "\n"
          end
          i+=1
        end
        print "\n"
        puts "-----------------------------------------------------------------------"
      end
    end 

    def get_filmes
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json';
      uri = URI(url);
      response = Net::HTTP.get(uri);
      return JSON.parse(response, symbolize_names: true);
    end
  end

  class Menu
    
    def initialize
      a=Operacoes.new
      print "------------SELECIONE--UMA--OPÇÃO-------------  \n";
      print "|               1-FILMES                      | \n";
      print "|               2-MEDIAS                      | \n";
      print "|               3-DIVISIVEL POR 25            | \n";
      print "|               4-Numeros quadrados perfeitos | \n"
      print "|               0-SAIR                        | \n";
      print "----------------------------------------------  \n";
      option = gets.chomp.to_i;
      case option
      when 1
        a.filtrar_filmes();
      when 2
        puts "medias";
        puts "escreva a lista de alunos e a nota EX: {'nome_do_aluno1':nota,'nome_do_aluno2':nota2}"
        var1= gets.chomp
        puts "escreva o nome dos alunos a serem retirados da media"
        var2= gets.chomp
        a.media_preconceituosa(var1,var2)
      when 3
        puts "escolha os numero(separados por espaço) e ao fim aperte enter";
        numeros = gets.chomp;
        a.sem_numeros(numeros);
      when 4 
        puts "escolha um numero:"
        var1 = gets.chomp.to_i
        a.numPerf(var1)
      when 0
        puts "sair";
        exit;
      else
        puts "opção invalida";
      end
    end
  end
end