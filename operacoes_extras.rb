module OperacoesExtras
    def quadPerf(x)
        p=program(x)
        puts "os numeros perfeitos de 1 a #{x} são:"
        print p
        print "\n"                 
    end
end
def sum d
    # sum of all elems in array d
    s = 0
    d.each { |i| s += i }
    s
end

def divisors (x)
    # array of all divisors of x except itself
    d = []
    for i in (1..x/2) do
        d << i if x%i == 0
    end
    d
end

def perfect (x)
    # x is perfect or not?
    d = divisors(x)
    x == sum(d)
end

def program(x) 
    # perfect numbers in range 1..x
    s = []
    for i in (1..x)
        s << i if perfect(i)
    end
    s
end
